class Arm(){
    var openAngle = 0.0
    var altitude = 0.0
    var turnedOn = false
    fun toggle(){
        turnedOn = !turnedOn
        showStatus()
    }
    fun updateAltitude(num: Int){
        if (turnedOn){
            altitude += num
            if (altitude >= 30.0){
                altitude = 30.0
            }
            else if (altitude <= 0.0){
                altitude = 0.0
            }
        }
        showStatus()
    }
    fun updateAngle(numangle: Int){
        if (turnedOn){
            openAngle += numangle
            if (openAngle >= 360.0){
                openAngle = 360.0
            }
            else if (openAngle <= 0.0){
                openAngle = 0.0
            }
        }
        showStatus()

    }
    override fun toString(): String {
        return "MechanicalArm{openAngle=$openAngle, altitude=$altitude, turnedOn=$turnedOn}"
    }
    fun showStatus(){
        println(toString())
    }
}

fun main(){
    val brazo = Arm()
    brazo.toggle()
    brazo.updateAltitude(3)
    brazo.updateAngle(180)
    brazo.updateAltitude(-3)
    brazo.updateAngle(-180)
    brazo.updateAltitude(3)
    brazo.toggle()
}
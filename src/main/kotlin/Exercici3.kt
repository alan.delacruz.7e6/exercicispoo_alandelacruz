import java.util.*

class Taulell(val preuUnitari: Double, val height: Double, val width: Double){
   fun preutotal(): Double{
       return preuUnitari * width * height
   }
}

class Llisto(val preuUnitari: Double, val llistoheight: Double){
    fun preutotal(): Double{
        return preuUnitari * llistoheight
    }
}
fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    val number = scanner.nextInt()
    var taulells = mutableListOf<Taulell>()
    var llistons = mutableListOf<Llisto>()
    for (i in 1..number){
        val type = scanner.next().lowercase()
        if (type == "taulell"){
            val preu = scanner.nextDouble()
            val height = scanner.nextDouble()
            val width = scanner.nextDouble()
            taulells.add(Taulell(preu, height, width))
        }
        else if (type == "llistó"){
            val preullisto = scanner.nextDouble()
            val heightllisto = scanner.nextDouble()
            llistons.add(Llisto(preullisto, heightllisto))
        }
    }
    println("El preu total és de: ${taulells.sumOf { it.preutotal() } + llistons.sumOf { it.preutotal() }}")
}


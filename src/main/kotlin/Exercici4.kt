import java.util.*

class Lampada(private val id: String) {
    val colors = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED, ANSI_RED, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN)
    var color = 1
    var intensitat = 1
    var intensitatPujada = true

    fun showColor(){
        println("Identificador: $id  Color: ${colors[color]}    ${ANSI_RESET} - intensitat $intensitat")
    }

    fun turnOn() {
        if (color == 0) {
            intensitat = 1
            intensitatPujada = true
        }
        showColor()
    }

    fun turnOff() {
        if (color > 0) {
            intensitat = 0
        }
        showColor()
    }

    fun changeColor() {
        if (color <= colors.size - 2) color++
        else if (color == colors.size - 2) color = 1
        showColor()
    }

    fun changeIntensity() {
        if (intensitatPujada) intensitat++
        else if (intensitatPujada && intensitat == 5) {
            intensitatPujada = false
            intensitat--
        } else if (!intensitatPujada && intensitat > 1) intensitat--
        else if (!intensitatPujada && intensitat == 1) {
            intensitat++
        }
        showColor()
    }
}

//definició dels colors que farem servir, agafant els backgrounds ANSI, i establint-los com a constants globals
const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WHITE = "\u001B[47m"

fun main() {
    val identificador = Lampada("Menjador")
    val id2 = Lampada("Cuina")
    identificador.turnOn()
    identificador.changeColor()
    for (i in 1..5){
        identificador.changeIntensity()
    }

    id2.turnOn()
    for (i in 1..2){
        id2.changeColor()
    }
    for (i in 1..5){
        id2.changeIntensity()
    }
    id2.turnOff()
    id2.changeColor()
    id2.turnOn()
    id2.changeColor()
    for (i in 1..5){
        id2.changeIntensity()
    }



}
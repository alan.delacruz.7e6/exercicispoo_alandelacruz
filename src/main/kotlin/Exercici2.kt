class Pastas(var nom: String, var preu: Double, var pes: String, var calories: Int) {
    fun buildinfo(): String {
        return "nom: $nom, preu: $preu, pes: $pes, calories: $calories"
    }
}

class Beguda(var nomb: String, var preub: Double, var ensucrada: Boolean) {
    init {
        if (ensucrada){
            preub *= 1.10
        }
    }
}

fun main(args: Array<String>) {
    val croissant = Pastas("croissant", 2.50, "130g", 406)
    val ensaimada = Pastas("ensaimada", 2.10, "100g", 350)
    val donut = Pastas("donut", 1.50, "52g", 200)
    println(croissant.buildinfo())
    println(ensaimada.buildinfo())
    println(donut.buildinfo())

    println("----------begudes-----------")

    val aigua = Beguda("Aigua", 1.00, false)
    val cafe = Beguda("Café tallat", 1.35, false)
    val te = Beguda("Té vermell", 1.50, false)
    val cola = Beguda("Cola", 1.65, true)
    val begudes = mutableListOf<Beguda>()
    begudes.add(aigua)
    begudes.add(cafe)
    begudes.add(te)
    begudes.add(cola)
    for (beguda in begudes){
        println("Nom beguda: ${beguda.nomb}, Preu: ${beguda.preub}€, ensucrada: ${beguda.ensucrada} ")
    }
}
open class Electrodomestic(
    open var preuBase: Double,
    open var color: String = "blanc",
    open var consum: Consum = Consum.G,
    open var pes: Double = 5.0
) {
    open fun showfirstInfo(pos: Int) {
        println("-Electrodomestic $pos\nPreu Base: $preuBase\nColor: $color\nConsum: $consum\nPes: $pes\nPreu Final: ${preuFinal()}")
    }

    open fun showInfo() {
        println("Electrodomèstics: \n${preuBase}\n${preuFinal()}")
    }

    open fun preuFinal(): Double {
        return preuBase + basePes()
    }

    fun basePes(): Double {
        return if (pes in 6.0..20.0) {
            20.0
        } else if (pes in 0.0..50.0) {
            50.0
        } else if (pes in 0.0..80.0) {
            80.0
        } else {
            100.0
        }
    }
}

data class Rentadora(
    var carrega: Int = 5,
    override var preuBase: Double,
    override var color: String,
    override var consum: Consum,
    override var pes: Double
) : Electrodomestic(preuBase, color, consum, pes) {

    override fun showfirstInfo(pos: Int){
        println("-Rentadora $pos\nCarrega: $carrega\nPreu Base: $preuBase\nColor: $color\nConsum: $consum\nPes: $pes\nPreu Final: ${preuFinal()}")
    }
    override fun showInfo() {
        println("Rentadores: \n${preuBase}\n${preuFinal()}")
    }

    override fun preuFinal(): Double {
        return preuBase + carregaBase()
    }

    fun carregaBase(): Double {
        return if (carrega == 6 || carrega == 7) {
            55.0
        } else if (carrega == 8) {
            70.0
        } else if (carrega == 9) {
            85.0
        } else {
            100.0
        }

    }
}

data class Televisio(
    var tamany: Int = 28,
    override var preuBase: Double,
    override var color: String,
    override var consum: Consum,
    override var pes: Double
) : Electrodomestic(preuBase, color, consum, pes) {

    override fun showfirstInfo(pos: Int){
        println("-Televisió $pos\nTamany: $tamany\nPreu Base: $preuBase\nColor: $color\nConsum: $consum\nPes: $pes\nPreu Final: ${preuFinal()}")
    }
    override fun showInfo() {
        println("Televisions: \n-${preuBase}\n-${preuFinal()}")
    }

    override fun preuFinal(): Double {
        return preuBase + tamanyBase()
    }

    fun tamanyBase(): Double {
        return if (tamany in 29..32) {
            50.0
        } else if (tamany in 0..42) {
            100.0
        } else if (tamany in 0..50) {
            150.0
        } else {
            200.0
        }
    }
}

enum class Consum() {
    A {
        override fun preuConsum(): Int {
            return 35
        }
    },
    B {
        override fun preuConsum(): Int {
            return 30
        }
    },
    C {
        override fun preuConsum(): Int {
            return 25
        }
    },
    D {
        override fun preuConsum(): Int {
            return 20
        }
    },
    E {
        override fun preuConsum(): Int {
            return 15
        }
    },
    F {
        override fun preuConsum(): Int {
            return 10
        }
    },
    G {
        override fun preuConsum(): Int {
            return 0
        }
    };

    abstract fun preuConsum(): Int
}

fun main() {
    var electrodomestics = mutableListOf<Electrodomestic>(
        Electrodomestic(400.0, "amarillo", Consum.A, 76.0),
        Electrodomestic(200.0, "azul", Consum.C, 176.0),
        Electrodomestic(500.0, "morado", Consum.B, 56.0),
        Electrodomestic(100.0, "gris", Consum.D, 16.0),
        Electrodomestic(300.0, "azul", Consum.E, 276.0),
        Electrodomestic(250.0, "azul", Consum.F, 26.0),
        Rentadora(5, 350.0, "negro", Consum.E, 70.0),
        Rentadora(8, 380.0, "crema", Consum.G, 90.0),
        Televisio(52, 500.0, "negro", Consum.C, 20.0),
        Televisio(28, 1000.0, "rojo", Consum.D, 15.0)
    )
    for (i in electrodomestics.indices){
        electrodomestics[i].showfirstInfo(i)
    }
    var totalbaseelectro = 0.0
    var totalbaserentadores = 0.0
    var totalbasetelevisions = 0.0

    var totalfinalelectro = 0.0
    var totalfinalrentadores = 0.0
    var totalfinaltelevisions = 0.0
    for (i in electrodomestics){
        when (i) {
            is Rentadora -> totalbaserentadores += i.preuBase
            is Televisio -> totalbasetelevisions += i.preuBase
            is Electrodomestic -> totalbaseelectro += i.preuBase
        }
    }
    for (j in electrodomestics){
        when (j){
            is Rentadora -> totalfinalrentadores += j.preuFinal()
            is Televisio -> totalfinaltelevisions += j.preuFinal()
            is Electrodomestic -> totalfinalelectro += j.preuFinal()
        }
    }
    println("---------------------------------")
    println("- Electrodomèstics: \n - Preu Base: $totalbaseelectro \n -Preu Final: $totalfinalelectro")
    println("- Rentadores: \n - Preu Base: $totalbaserentadores \n -Preu Final: $totalfinalrentadores")
    println("- Televisions: \n - Preu Base: $totalbasetelevisions \n -Preu Final: $totalfinaltelevisions")
}
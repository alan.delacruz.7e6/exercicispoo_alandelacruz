class Pasta(var nom: String, var preu: Double, var pes: String, var calories: Int) {
    fun buildinfo(): String {
        return "nom: $nom, preu: $preu, pes: $pes, calories: $calories"
    }
}

fun main(args: Array<String>) {
    val croissant = Pasta("croissant", 2.50, "130g", 406)
    val ensaimada = Pasta("ensaimada", 2.10, "100g", 350)
    val donut = Pasta("donut", 1.50, "52g", 200)
    println(croissant.buildinfo())
    println(ensaimada.buildinfo())
    println(donut.buildinfo())

}